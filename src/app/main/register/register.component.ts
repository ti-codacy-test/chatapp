import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import {ApiService} from '../../services/api.service'
import { HttpHeaders } from '@angular/common/http';
import { User } from 'src/app/classes/user';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  private user: User;
  signupForm: FormGroup
  constructor(
    private _api : ApiService,
    private _auth: AuthService,
    private router: Router,
    public fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.createForm();
  }
  createForm(): void {
    this.signupForm = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  register(){
    let b = this.signupForm.value
    this._api.postTypeRequest('register', b).subscribe((res: any) => {
      console.log(res)
      if(res){
        this._auth.setDataInLocalStorage('token', res.access_token);
        alert('User is Registerd');
        this.router.navigate(['login'])
      }
    }, err => {
      console.log(err)
    });
  }

}
