import { Component, Input, OnInit } from '@angular/core';
import { Message } from '../../../../classes/message';
@Component({
  selector: 'app-chat-message',
  templateUrl: './chat-message.component.html',
  styleUrls: ['./chat-message.component.scss']
})
export class ChatMessageComponent {

  @Input() message: Message;


}
