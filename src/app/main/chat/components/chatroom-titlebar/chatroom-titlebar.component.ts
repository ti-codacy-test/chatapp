import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-chatroom-titlebar',
  templateUrl: './chatroom-titlebar.component.html',
  styleUrls: ['./chatroom-titlebar.component.scss']
})
export class ChatroomTitlebarComponent {

  @Input() title: string;
}
